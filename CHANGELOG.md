
# 202405-07

* update FO[PF] to reflect the final version of [IEP 23](https://gitlab.com/krr/IDP-Z3/-/wikis/IEP-23-Type-and-domain-checks) (See [Steerco](https://gitlab.com/krr/IDP-Z3/-/wikis/SteerCo-2024/04/SteerCo-2024-04-29))

# 2024-04-05

* major update of FO[PF]
* replace `POINT` by `INTEGER`

# 2023-08-30

* finalize FO(PF)

# 2023-05-15

* update syntax of FO(Agg)