Rationale
=========

**What is the knowledge base paradigm and why do we need it ?**

Declarative knowledge and its use for solving problems is studied in
various fields such as knowledge representation and reasoning (KRR), computational logic and declarative
problem solving paradigms. In the past half a century, an immense body
of scientific knowledge about knowledge and its use for solving problems
has been collected in these various domains. Yet, at this moment no
coherent scientific approach to the study of knowledge and its use for
problem solving has emerged. Scientific understanding is
scattered over the many fields that study it.

One issue that fragments the study of knowledge and reasoning
more than anything else is the type of inference (the type of reasoning
with this knowledge). Historically, the  study of logic was seen as the
study of reasoning. This inference-centric view naturally leads to
different logics specialized on
specific forms of inference.  To start with,  classical first order
logic (FO) is sometimes defined as  the logic of deductive reasoning.
But  database languages (SQL, Datalog)  are logics for query answering;
constraint programming (CP) for constraint solving; answer set
programming (ASP) for solving problems by
computing answer sets, temporal logics for model checking, etc. All
these logics differ strongly at the level
of the underlying inference mechanism as well as on their syntax and on
their scientific  terminologies.

But knowledge is independent of the sort of reasoning task.  Take the
proposition:

> During opening hours, at least one secretary is present at the entrance
desk.

What task is this proposition to be used for? It could be used as a
query to a scheduling database, or as a
constraint in a scheduling problem, or as a property to be verified from
a formal specification of a scheduling
problem, or as a correctness property to be proven of a program that
computes schedules. The proposition in itself is not bound to any
specific problem or form of inference; yet, in the current state of the
art, depending on the problem, the proposition needs to be rephrased in
a logic that supports the required type of inference.

In fact, the proposition has a natural representation as the sentence in
first order logic:

            ∀t : Open(t) ⇒ ∃s : Secretary(s) ∧ at(s, EntranceDesk, t).

Assume we want to solve a scheduling problem satisfying this formula.
For a long time a “deductive” logic
like FO was deemed unsuitable for such problem. Often this was blamed on
the fact that deduction in FO is
undecidable. However, the more essential reason is that the scheduling
problem is simply not a deductive problem and  FO theorem provers are
useless.  Instead, other declarative paradigms were developed for this
such as CP.

The mix up of reasoning with the knowledge representation language is
not a desirable situation; neither from a scientific point of view as it
obscures the nature of knowledge and obfuscates it with the form of
reasoning, nor from a pragmatical point of view as it forces the
knowledge engineer to rephrase the same proposition in different logics
to get different problems solved.  What is needed is a
knowledge-oriented scientific approach to study knowledge separated
from  inference and problem solving, that allows to study knowledge with
scientific methods, that studies how to  express knowledge as naturally
and compactly as possible, and that studies the various sorts of
problems that  can be solved using a knowledge base. It leads to the
idea to build software solutions by expressing domain  knowledge in a
symbolic knowledge base, and use it to solve a range of problems and
tasks by applying various  inference methods. Such a system is what we
understand by the term ``a knowledge base system''.


**Why start from  classical first order logic (FO)?**

Classical first order logic   dates from the late nineteenth century.
Why use that logic? Because it's language constructs $\land, \lor, \neg,
\Rightarrow , \Leftrightarrow, \forall, \exists$ are basic indispensable construct for
expressing human knowledge. Moreover, FO's formal semantics correctly
captures their meaning.

**Why  extend FO?**

Classical language is really a very small language, with only 7 language
constructs. It can be improved for knowledge representation in many
different ways.
