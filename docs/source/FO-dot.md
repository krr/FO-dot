FO[·]
=====

This document describes the FO[·] (aka FO-dot) standard.

FO[·] is First Order logic with various extensions to allow complex knowledge to be expressed in a rigorous and elaboration-tolerant way.
FO[·] facilitates the development of knowledge-intensive applications capable of intelligent behavior {cite}`carbonnelle2022idpz3`.

The document is meant to be a reference for Knowledge engineers and developers of reasoning engines.

Suggestions for improvements to the document can be made via [issues on GitLab](https://gitlab.com/krr/FO-dot/-/issues).


```{toctree}
---
   maxdepth: 1
   caption: Contents
---
Introduction
Rationale
Notation
Core
Sugar
PF
ID
Int
Real
Agg
Infinite
Concept
Unit
Dot
biblio
genindex
```

