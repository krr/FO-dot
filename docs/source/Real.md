````{index} FO[Real]
````
FO[Real]
=======

**Goal**

The goal of FO[Real] is to allow expressing knowledge about real quantities using the 4 arithmetic operators and comparisons.

**Lexicon**


| Token | Pattern | Example |
| ----- | ------- | --------|
| DIGIT | `\d`    | |
| REAL   | `Real\|ℝ` | |
| REALVAL | `[+-]?\d+(\.\d*(e[+-]?\d+))?` | `-0.01e-3`|


**Syntax**

FO[Real] adds these production rules:

```
    Type ← REAL;
    Identifier ← REALVAL;
```

```{mermaid}
    flowchart LR
        subgraph Identifier
            direction LR
            start{ } --> SUM_MINUS;
            start --> DIGIT;
            SUM_MINUS --> DIGIT;
            DIGIT --> DIGIT;
            DIGIT --> END{ };
            DIGIT --> dot["'.'"];
            dot --> END;
            dot --> DIGIT2[DIGIT];
            DIGIT2 --> DIGIT2;
            DIGIT2 --> END;
            DIGIT2 -->e["'e'"];
            e --> sum2[SUM_MINUS];
            e --> DIGIT3[DIGIT];
            sum2 --> DIGIT3;
            DIGIT3 --> DIGIT3;
            DIGIT3 --> END;
        end;
```

**Well-formedness**

1. Division must be well-guarded to prevent division by 0
   (e.g., `if y ≠ 0 then x/y else 1.0`).
2. An arithmetic expression cannot mix integer and real numbers.
3. Quantification over ℝ is not allowed (relaxed in FO[Infinite]).

**Semantics**

The arithmetic and comparison operators have their usual meaning.

TODO: discuss approximations