"""
to be run before a public release, so that the PDF file is generated with correct math characters.

* it replaces greek letters by their latex code (e.g., \sigma)
* it replaces other math symbols by commands (e.g, \I)
    * HTML: macros can be defined in sphinx's [mathjax3_config](https://www.sphinx-doc.org/en/master/usage/extensions/math.html#confval-mathjax3_config)
      to replace by UTF-8 char, as in [here](https://docs.mathjax.org/en/v1.0/configuration.html#configuration-objects)
    * Latex: newcommands can be defined in sphinx [preamble](https://www.sphinx-doc.org/en/master/latex.html)
      to execute the command

use -reverse to reverse it.

TODO
"""